#include <DcMotor.h>
#include <Wire.h>
#include "MCP23008.h"

// direcciones I2C de los componentes
#define DIR_I2C_GESTION   0x27
#define DIR_I2C_SENSORES  0x21

// estados para la programación basada en autómatas
#define PARADO                0
#define CARRERA               1
#define LEER_MARCA_DERECHA    2
#define LEER_MARCA_IZQUIERDA  3
#define GIRAR_DERECHA         4
#define GIRAR_IZQUIERDA       5

// pines del arduino
#define MOTOR_DER_DIR     4
#define MOTOR_DER_VEL     5
#define MOTOR_IZQ_DIR     12
#define MOTOR_IZQ_VEL     6

// constantes para el control PD y el ajuste de la velocidad de los motores
#define KP                6
#define KD                15
#define VELOCIDAD         40

// Color de la linea
#define NEGRO   1
#define BLANCO  0

// instancia de la placa de control
MCP23008 gestionI2C;
MCP23008 sensoresI2C;

// instancia de los motores
DcMotor motorDerecha;
DcMotor motorIzquierda;


// variables
unsigned char estado = PARADO;
int error = 0, error_anterior;
int desfase;
bool sobreMarca = false, sobreMarcaAnterior;
int sensor_error[8] = { -8, -6, -4, -2, 2, 4, 6, 8};
unsigned long startTime;

char values;
int num_negros;
int velocidad_der;
int velocidad_izq;


void setup() {

  Serial.begin(9600);

  motorDerecha.stop();                       //detiene los motores
  motorIzquierda.stop();
  motorDerecha.begin(MOTOR_DER_DIR, MOTOR_DER_VEL);
  motorIzquierda.begin(MOTOR_IZQ_DIR, MOTOR_IZQ_VEL);

  gestionI2C.begin(DIR_I2C_GESTION);
  gestionI2C.pinMode(0x0F);
  gestionI2C.setPullup(0x0F);

  sensoresI2C.begin(DIR_I2C_SENSORES);
  sensoresI2C.pinMode(0xFF);
  sensoresI2C.setPullup(0x00);

  startTime = millis();
}

void loop() {


  switch (estado) {
    case PARADO: {
        // comprobación del botón de cambio de estado
        if (botonGrandePulsado() == true) {
          gestionI2C.write(4, HIGH);
          estado = CARRERA;                          // cambia al estado de carrera
          Serial.println("Pasa a estado de CARRERA");
        }
        break;
      }
    case CARRERA: {
        // comprobación del botón de cambio de estado
        if (botonGrandePulsado() == true) {
          parar();
          break;                                    // se rompe el switch para que el control PD no active los motores de nuevo
        }

        // lectura de sensores
        values = sensoresI2C.read();
        Serial.println("Comienza proceso de ajuste de control PD:");
        Serial.print("Lectura de sensores: ");
        Serial.println(values, BIN);

        sobreMarcaAnterior = sobreMarca; //estado anterior del sensor
        sobreMarca = false; //estado actual del sensor
        //si los valores de el sensor de la derecha del todo (0) o el contiguo (1) o los dos detectan HIGH (NEGRO) y lo detectan durante mas de 100ms esta sobre
        //la marca de giro a la derecha cambiando su estado y un break para terminar el estado actual para pasar directamete.
        /* if ((millis() >= (startTime + 100)) && (bitRead(values, 0) == HIGH || (bitRead(values, 0) == HIGH && bitRead(values, 1) == HIGH))) {
           startTime = millis();
           sobreMarca = true;
           estado = LEER_MARCA_DERECHA;
           Serial.println("Pasa a estado de LEER_MARCA_DERECHA");
           Serial.println((String)"marca actual " + sobreMarca);
           Serial.println((String)"marca anterior " + sobreMarcaAnterior);
           break;
          }*/
        //si los valores de el sensor de la izquierda del todo (7) o el contiguo (6) o los dos detectan HIGH (NEGRO) y lo detectan durante mas de 100ms esta sobre
        //la marca de giro a la izquerda cambiando su estado y un break para terminar el estado actual para pasar directamete.
        if ((millis() >= (startTime + 100)) && (bitRead(values, 7) == HIGH || (bitRead(values, 7) == HIGH && bitRead(values, 6) == HIGH))) {
          startTime = millis();
          sobreMarca = true;
          estado = LEER_MARCA_IZQUIERDA;
          Serial.println("Pasa a estado de LEER_MARCA_DERECHA");
          Serial.println((String)"marca actual " + sobreMarca);
          Serial.println((String)"marca anterior " + sobreMarcaAnterior);
          break;
        }

        // cálculo del error
        error_anterior = error;                     // se guarda el error anterior para el control derivativo
        error = 0;                                  // error y número de sensores en negro se inician en 0

        num_negros = 0;
        for (int i = 0; i < 8; i++) {
          if (bitRead(values, i) == HIGH) {         // si el sensor ha detectado negro,
            error += sensor_error[i];               // se suma el error correspondiente a ese sensor y
            num_negros++;                           // se incrementa el número de sensores que detectan negro
          }
        }
        calculoError();

        break;
      }
    /* case LEER_MARCA_DERECHA:
         if (botonGrandePulsado() == true) {
           parar();
           break;                                    // se rompe el switch para que el control PD no active los motores de nuevo
         }
         // lectura de sensores
         values = sensoresI2C.read();

         sobreMarcaAnterior = sobreMarca;
         sobreMarca = true; //en este estado la marca actual sera siempre TRUE (NEGRO) porque estamos sobre la marca derecha
         Serial.println((String)"marca actual " + sobreMarca);
         Serial.println((String)"marca anterior " + sobreMarcaAnterior);
         //si los sensores de la derecha del todo (0 y 1) detectan un LOW (BLANCO) la marca actual pasara a FALSE (BLANCO)
         if ((millis() >= (startTime + 100)) && (bitRead(values, 0) == LOW && bitRead(values, 1) == LOW)) {
           startTime = millis();
           sobreMarca = false;
         }

         //si el estado anterior de el sensor es TRUE (NEGRO) y el actual FALSE (BLANCO) quiere decir que ha terminado de leer la marca derecha
         //y pasara al estado girar derecha. Ponemos los dos estados a FALSE porque en el siguiente estado usarmeos los sensores del otro extemo y ahí empiezan en blanco.
         //Para finalizar, hacemos un break para forzar el cambio de estado.
         if (sobreMarcaAnterior && !sobreMarca) {
           estado = GIRAR_DERECHA;
           sobreMarcaAnterior = false;
           sobreMarca = false;
           Serial.println("pasar a estado GIRAR_DERECHA");
           break;
         }

         // cálculo del error
         error_anterior = error;                     // se guarda el error anterior para el control derivativo
         error = 0;                                  // error y número de sensores en negro se inician en 0

         num_negros = 0;
         for (int i = 2; i < 6; i++) {
           if (bitRead(values, i) == HIGH) {         // si el sensor ha detectado negro,
             error += sensor_error[i];               // se suma el error correspondiente a ese sensor y
             num_negros++;                           // se incrementa el número de sensores que detectan negro
           }
         }
         calculoError();
         break;

      case GIRAR_DERECHA: {
         Serial.println("GIRAR_DERECHA");
         if (botonGrandePulsado() == true) {
           parar();
           break;                                    // se rompe el switch para que el control PD no active los motores de nuevo
         }
         // lectura de sensores
         values = sensoresI2C.read();

         sobreMarcaAnterior = sobreMarca;
         sobreMarca = false; //en este caso esta en FALSE (BLANCO) porque queremos detectar el flanco descendente, es decir de HIGH (NEGRO) a LOW (BLANCO).
         Serial.println((String)"marca actual " + sobreMarca);
         Serial.println((String)"marca anterior " + sobreMarcaAnterior);
         //Primero tenenmos que detectar la marca de la bifurcacion izquierda con el sensor de la izquierda del todo.
         //Si lo detectamos durante mas de 100ms estamos sobre la marca y el sensor de la izquierda del todo (7) detecta un HIGH (NEGRO) esta sobre la marca TRUE (NEGRO)
         if ((millis() >= (startTime + 100)) && (bitRead(values, 7) == HIGH)) {
           startTime = millis();
           sobreMarca = true;
         }

         //si la marca anterior es TRUE (NEGRO) y la marca actual es TRUE (BLANCO) quiere decir que ha terminado de hacer el giro y cambiaremos el estado a carrera.
         //este estado ocurrira cuando deje de detectar la bifurcacion a la izquierda en el giro a la derecha.
         //Para finalizar hacemos un break para forzar el cambio de estado
         if (sobreMarcaAnterior && !sobreMarca) {
           estado = CARRERA;
           Serial.println("pasar a estado CARRERA");
           break;
         }

         // cálculo del error
         error_anterior = error;                     // se guarda el error anterior para el control derivativo
         error = 0;                                  // error y número de sensores en negro se inician en 0

         num_negros = 0;
         for (int i = 0; i < 4; i++) {
           if (bitRead(values, i) == HIGH) {         // si el sensor ha detectado negro,
             error += sensor_error[i];               // se suma el error correspondiente a ese sensor y
             num_negros++;                           // se incrementa el número de sensores que detectan negro
           }
         }
         calculoError();
         break;
  */case LEER_MARCA_IZQUIERDA:
      if (botonGrandePulsado() == true) {
        parar();
        break;                                    // se rompe el switch para que el control PD no active los motores de nuevo
      }
      // lectura de sensores
      Serial.println("LEER_MARCA_IZQUIERDA");
      Serial.println("Comienza proceso de ajuste de control PD:");
      Serial.print("Lectura de sensores: ");
      values = sensoresI2C.read();

      sobreMarcaAnterior = sobreMarca;
      sobreMarca = true; //en este estado la marca actual sera siempre TRUE (NEGRO) porque estamos sobre la marca derecha
      Serial.println((String)"marca actual " + sobreMarca);
      Serial.println((String)"marca anterior " + sobreMarcaAnterior);
      //si los sensores de la derecha del todo (0 y 1) detectan un LOW (BLANCO) la marca actual pasara a FALSE (BLANCO)
      if ((millis() >= (startTime + 100)) && (bitRead(values, 7) == LOW && bitRead(values, 6) == LOW)) {
        startTime = millis();
        sobreMarca = false;
      }

      //si el estado anterior de el sensor es TRUE (NEGRO) y el actual FALSE (BLANCO) quiere decir que ha terminado de leer la marca izquierda
      //y pasara al estado girar izquierda. Ponemos los dos estados a FALSE porque en el siguiente estado usarmeos los sensores del otro extemo y ahí empiezan en blanco.
      //Para finalizar, hacemos un break para forzar el cambio de estado.
      if (sobreMarcaAnterior && !sobreMarca) {
        estado = GIRAR_IZQUIERDA;
        sobreMarcaAnterior = false;
        sobreMarca = false;
        Serial.println("pasar a estado GIRAR_IZQUIERDA");
        break;
      }

      // cálculo del error
      error_anterior = error;                     // se guarda el error anterior para el control derivativo
      error = 0;                                  // error y número de sensores en negro se inician en 0

      num_negros = 0;
      for (int i = 2; i < 6; i++) {
        if (bitRead(values, i) == HIGH) {         // si el sensor ha detectado negro,
          error += sensor_error[i];               // se suma el error correspondiente a ese sensor y
          num_negros++;                           // se incrementa el número de sensores que detectan negro
        }
      }
      calculoError();
      break;

    case GIRAR_IZQUIERDA:
      if (botonGrandePulsado() == true) {
        parar();
        break;                                    // se rompe el switch para que el control PD no active los motores de nuevo
      }
      // lectura de sensores
      Serial.println("GIRAR_IZQUIERDA");
      Serial.println("Comienza proceso de ajuste de control PD:");
      Serial.print("Lectura de sensores: ");
      values = sensoresI2C.read();

      sobreMarcaAnterior = sobreMarca;
      sobreMarca = false; //en este caso esta en FALSE (BLANCO) porque queremos detectar el flanco descendente, es decir de HIGH (NEGRO) a LOW (BLANCO).
      Serial.println((String)"marca actual " + sobreMarca);
      Serial.println((String)"marca anterior " + sobreMarcaAnterior);
      //Primero tenenmos que detectar la marca de la bifurcacion derecha con el sensor de la derecha del todo.
      //Si lo detectamos durante mas de 100ms estamos sobre la marca y el sensor de la izquierda del todo (7) detecta un HIGH (NEGRO) esta sobre la marca TRUE (NEGRO)
      if ((millis() >= (startTime + 100)) && (bitRead(values, 0) == HIGH)) {
        startTime = millis();
        sobreMarca = true;
      }

      //si la marca anterior es TRUE (NEGRO) y la marca actual es FALSE (BLANCO) quiere decir que ha terminado de hacer el giro y cambiaremos el estado a carrera.
      //este estado ocurrira cuando deje de detectar la bifurcacion a la derecha en el giro a la izquierda.
      //Para finalizar hacemos un break para forzar el cambio de estado
      if (sobreMarcaAnterior && !sobreMarca) {
        estado = CARRERA;
        Serial.println("pasar a estado CARRERA");
        break;
      }

      // cálculo del error
      error_anterior = error;                     // se guarda el error anterior para el control derivativo
      error = 0;                                  // error y número de sensores en negro se inician en 0

      num_negros = 0;
      for (int i = 3; i < 8; i++) {
        if (bitRead(values, i) == HIGH) {         // si el sensor ha detectado negro,
          error += sensor_error[i];               // se suma el error correspondiente a ese sensor y
          num_negros++;                           // se incrementa el número de sensores que detectan negro
        }
      }
      calculoError();
      break;

  }
  delay(5);                                       // tiempo de espera hasta la próxima aplicación del control PD
  // descomentar el siguiente delay para debuguear y que los mensajes de puerto serie salgan mas despacio
  //delay(500);
}

unsigned char estado_anterior_boton = HIGH;

// funcion que detecta la pulsacion del boton y su flanco descendente
bool botonGrandePulsado() {

  // lectura del estado del botón
  char estado_boton = gestionI2C.read(3);
  // identificación de un flanco ascendente en base al estado anterior y al actual
  if ((estado_anterior_boton == HIGH) && (estado_boton == LOW)) {
    estado_anterior_boton = estado_boton;
    return true;
  }
  else {
    estado_anterior_boton = estado_boton;
    return false;
  }

}
//funcion que cambia al estado de PARADO
void parar() {
  gestionI2C.write(4, LOW);
  estado = PARADO;                           // cambia al estado de parada
  motorDerecha.stop();                       //detiene los motores
  motorIzquierda.stop();
  Serial.println("Pasa a estado de PARADO");
}
//funcion que calcula el error para rastrear y mover os motores
void calculoError() {
  if (num_negros != 0) {                      // sólo si se ha detectado algún negro,
    error /= num_negros;                      // se aplica la corrección al error para el caso de uno o dos sensores detectando negro
  }
  Serial.println((String)"Error calculado: " + error);
  Serial.println((String)"Error anterior: " + error_anterior);

  // aplicación del control PD
  desfase = KP * error + KD * (error - error_anterior);

  // cálculo y limitación de las velocidades
  velocidad_izq = VELOCIDAD + desfase;    // se calcula la nueva velocidad para el motor izquierdo y
  if (velocidad_izq < 0) {                    // se comprueba que la velocidad esté dentro del rango de 0 a 255.
    velocidad_izq = 0;
  }
  else if (velocidad_izq > 255) {
    velocidad_izq = 255;
  }
  velocidad_der = VELOCIDAD - desfase;    // se calcula la nueva velocidad para el motor derecho y
  if (velocidad_der < 0) {                    // se comprueba que la velocidad esté dentro del rango de 0 a 255.
    velocidad_der = 0;
  }
  else if (velocidad_der > 255) {
    velocidad_der = 255;
  }
  //actualización de velocidad en motores (comentados para que no se mueva el coche)
  motorDerecha.move(FORWARD, velocidad_der);
  motorIzquierda.move(FORWARD, velocidad_izq);

}

#include <DcMotor.h>
#include <Wire.h>
#include "MCP23008.h"

// direcciones I2C de los componentes
#define DIR_I2C_GESTION   0x27
#define DIR_I2C_SENSORES  0x21

// estados para la programación basada en autómatas
#define PARADO                0
#define CARRERA               1
#define VERIFICAR_IZQ         2
#define VERIFICAR_DER         3
#define LEER_MARCA_DERECHA    4
#define LEER_MARCA_IZQUIERDA  5
#define GIRAR_DERECHA         6
#define GIRAR_IZQUIERDA       7

// pines del arduino
#define MOTOR_DER_DIR     4
#define MOTOR_DER_VEL     5
#define MOTOR_IZQ_DIR     12
#define MOTOR_IZQ_VEL     6

// constantes para el control PD y el ajuste de la velocidad de los motores
#define KP                7
#define KD                15
#define VELOCIDAD         70

#define debug Serial1 //constante para cambiar de mensajes bluethoot a ordenador

// instancia de la placa de control
MCP23008 gestionI2C;
MCP23008 sensoresI2C;

// instancia de los motores
DcMotor motorDerecha;
DcMotor motorIzquierda;


// variables
unsigned char estado = PARADO;
int error = 0, error_anterior;
int desfase;
bool sobreMarca = false, sobreMarcaAnterior;
int sensor_error[8] = { -16, -8, -4, -2, 2, 4, 8, 16};
unsigned long startTime;

char values;
int num_negros;
int velocidad_der;
int velocidad_izq;



void setup() {

  debug.begin(9600);

  motorDerecha.stop();
  motorIzquierda.stop();
  motorDerecha.begin(MOTOR_DER_DIR, MOTOR_DER_VEL);
  motorIzquierda.begin(MOTOR_IZQ_DIR, MOTOR_IZQ_VEL);

  gestionI2C.begin(DIR_I2C_GESTION);
  gestionI2C.pinMode(0x0F);
  gestionI2C.setPullup(0x0F);

  sensoresI2C.begin(DIR_I2C_SENSORES);
  sensoresI2C.pinMode(0xFF);
  sensoresI2C.setPullup(0x00);

  startTime = millis();
}

void loop() {
  switch (estado) {
    //----------------------PARADO------------------------------
    case PARADO:
      // comprobación del botón de cambio de estado
      if (botonGrandePulsado() == true) {
        gestionI2C.write(4, HIGH);
        estado = CARRERA;
        debug.println("-> Cambio a estado CARRERA");
      }
      //--------------------BLUETOOTH----------------------
      if (debug.available() > 0) {
        char dato_entrada = debug.read();
        if (dato_entrada == 's') {
          gestionI2C.write(4, HIGH);
          estado = CARRERA;
          debug.println("-> Cambio a estado CARRERA");
          break;
        }
      }
      break;
    //----------------------CARRERA------------------------------
    case CARRERA:
      // comprobación del botón de cambio de estado
      if (botonGrandePulsado() == true) {
        parar();
        break;
      }
      //--------------------BLUETOOTH----------------------
      if (debug.available() > 0) {
        char dato_entrada = debug.read();
        if (dato_entrada == 'f') {
          parar();
          break;
        }
      }

      // lectura de sensores
      debug.println("CARRERA");
      values = sensoresI2C.read();

      //lectura marca izquierda
      if ((bitRead(values, 7) == HIGH && bitRead(values, 6) == LOW && (bitRead(values, 4) == HIGH ||bitRead(values, 3) == HIGH)) || (bitRead(values, 6) == HIGH && bitRead(values, 5) == LOW && (bitRead(values, 4) == HIGH ||bitRead(values, 3) == HIGH))) {
        startTime = millis();
        estado = VERIFICAR_IZQ;
        break;
      }

      //lectura marca derecha
      if ((bitRead(values, 0) == HIGH && bitRead(values, 1) == LOW && (bitRead(values, 4) == HIGH ||bitRead(values, 3) == HIGH)) || (bitRead(values, 1) == HIGH && bitRead(values, 2) == LOW && (bitRead(values, 4) == HIGH ||bitRead(values, 3) == HIGH)))  {
        startTime = millis();
        estado = VERIFICAR_DER;
        break;
      }

      calculoError(0, 8);

      break;
    //----------------------VERIFICAR IZQUIERDA------------------------------
    case VERIFICAR_IZQ:
      if (botonGrandePulsado() == true) {
        parar();
        break;
      }
      //--------------------BLUETOOTH----------------------
      if (debug.available() > 0) {
        char dato_entrada = debug.read();
        if (dato_entrada == 'f') {
          parar();
          break;
        }
      }
      // si la marca izquierda es verdadera
      if (millis() >= (startTime + 200)) {
        if ((bitRead(values, 7) == HIGH && bitRead(values, 6) == LOW) || (bitRead(values, 6) == HIGH && bitRead(values, 5) == LOW)) {
          estado = LEER_MARCA_IZQUIERDA;
          break;
        }
        else {
          estado = CARRERA;
          break;
        }
      }
      break;

    //----------------------VERIFICAR DERECHA------------------------------
    case VERIFICAR_DER:
      if (botonGrandePulsado() == true) {
        parar();
        break;
      }
      //--------------------BLUETOOTH----------------------
      if (debug.available() > 0) {
        char dato_entrada = debug.read();
        if (dato_entrada == 'f') {
          parar();
          break;
        }
      }
      // si la marca derecha es verdadera
      if (millis() >= (startTime + 200)) {
        if  ((bitRead(values, 0) == HIGH && bitRead(values, 1) == LOW) || (bitRead(values, 1) == HIGH && bitRead(values, 2) == LOW)) {
          estado = LEER_MARCA_DERECHA;
          break;
        }
        else {
          estado = CARRERA;
          break;
        }
      }
      break;
    //----------------------LEER MARCA IZQUIERDA------------------------------
    case LEER_MARCA_IZQUIERDA:
      if (botonGrandePulsado() == true) {
        parar();
        break;
      }
      //--------------------BLUETOOTH----------------------
      if (debug.available() > 0) {
        char dato_entrada = debug.read();
        if (dato_entrada == 'f') {
          parar();
          break;
        }
      }
      // lectura de sensores
      debug.println("LEER_MARCA_IZQUIERDA");
      values = sensoresI2C.read();

      //si los sensores de la izquierda del todo (7 y 6) detectan un LOW (BLANCO) cambia a estado girar izquierda
      if (bitRead(values, 7) == LOW && bitRead(values, 6) == LOW) {
        estado = GIRAR_IZQUIERDA;
        sobreMarcaAnterior = false;
        sobreMarca = false;
        debug.println("-> Cambio a estado GIRAR_IZQUIERDA");
        break;
      }
      calculoError(2, 6);
      break;

    //----------------------LEER MARCA DERECHA------------------------------
    case LEER_MARCA_DERECHA:
      if (botonGrandePulsado() == true) {
        parar();
        break;
      }
      //--------------------BLUETOOTH----------------------
      if (debug.available() > 0) {
        char dato_entrada = debug.read();
        if (dato_entrada == 'f') {
          parar();
          break;
        }
      }
      // lectura de sensores
      debug.println("LEER_MARCA_DERECHA");
      values = sensoresI2C.read();

      //si los sensores de la derecha del todo (0 y 1) detectan un LOW (BLANCO) cambia a estado girar derecha
      if (bitRead(values, 0) == LOW && bitRead(values, 1) == LOW) {
        estado = GIRAR_DERECHA;
        sobreMarcaAnterior = false;
        sobreMarca = false;
        debug.println("-> Cambio a estado GIRAR_DERECHA");
        break;
      }
      calculoError(2, 6);
      break;
    //----------------------GIRAR IZQUIERDA------------------------------
    case GIRAR_IZQUIERDA:
      if (botonGrandePulsado() == true) {
        parar();
        break;
      }
      //--------------------BLUETOOTH----------------------
      if (debug.available() > 0) {
        char dato_entrada = debug.read();
        if (dato_entrada == 'f') {
          parar();
          break;
        }
      }
      // lectura de sensores
      debug.println("GIRAR_IZQUIERDA");
      values = sensoresI2C.read();

      sobreMarcaAnterior = sobreMarca;
      sobreMarca = false; //en este caso esta en FALSE (BLANCO) porque queremos detectar el flanco descendente, es decir de HIGH (NEGRO) a LOW (BLANCO).
      //Primero tenenmos que detectar la marca de la bifurcacion derecha con el sensor de la derecha del todo.
      //si el sensor de la derecha del todo (0) detecta un HIGH (NEGRO) esta sobre la marca TRUE (NEGRO)
      if (bitRead(values, 0) == HIGH) {
        startTime = millis();
        sobreMarca = true;
      }

      //si la marca anterior es TRUE (NEGRO) y la marca actual es FALSE (BLANCO) quiere decir que ha terminado de hacer el giro y cambiaremos el estado a carrera.
      //este estado ocurrira cuando deje de detectar la bifurcacion a la derecha en el giro a la izquierda.
      //Para finalizar hacemos un break para forzar el cambio de estado
      if (sobreMarcaAnterior && !sobreMarca) {
        estado = CARRERA;
        debug.println("-> Cambio a estado CARRERA");
        break;
      }
      calculoError(3, 8);
      break;
    //----------------------GIRAR DERECHA------------------------------
    case GIRAR_DERECHA:
      if (botonGrandePulsado() == true) {
        parar();
        break;
      }
      //--------------------BLUETOOTH----------------------
      if (debug.available() > 0) {
        char dato_entrada = debug.read();
        if (dato_entrada == 'f') {
          parar();
          break;
        }
      }
      // lectura de sensores
      debug.println("GIRAR_DERECHA");
      values = sensoresI2C.read();

      sobreMarcaAnterior = sobreMarca;
      sobreMarca = false; //en este caso esta en FALSE (BLANCO) porque queremos detectar el flanco descendente, es decir de HIGH (NEGRO) a LOW (BLANCO).
      //Primero tenenmos que detectar la marca de la bifurcacion izquierda con el sensor de la izquierda del todo.
      //si el sensor de la izquierda del todo (7) detecta un HIGH (NEGRO) esta sobre la marca TRUE (NEGRO)
      if (bitRead(values, 7) == HIGH) {
        startTime = millis();
        sobreMarca = true;
      }

      //si la marca anterior es TRUE (NEGRO) y la marca actual es FALSE (BLANCO) quiere decir que ha terminado de hacer el giro y cambiaremos el estado a carrera.
      //este estado ocurrira cuando deje de detectar la bifurcacion a la derecha en el giro a la izquierda.
      //Para finalizar hacemos un break para forzar el cambio de estado
      if (sobreMarcaAnterior && !sobreMarca) {
        estado = CARRERA;
        debug.println("-> Cambio a estado CARRERA");
        break;
      }
      calculoError(0, 6);
      break;

  }
  delay(5);
  //delay(500);
}

unsigned char estado_anterior_boton = HIGH;

// funcion que detecta la pulsacion del boton y su flanco descendente
bool botonGrandePulsado() {

  // lectura del estado del botón
  char estado_boton = gestionI2C.read(3);
  // identificación de un flanco ascendente en base al estado anterior y al actual
  if ((estado_anterior_boton == HIGH) && (estado_boton == LOW)) {
    estado_anterior_boton = estado_boton;
    return true;
  }
  else {
    estado_anterior_boton = estado_boton;
    return false;
  }

}

//funcion que cambia al estado de PARADO
void parar() {
  gestionI2C.write(4, LOW);
  estado = PARADO;                           // cambia al estado de parada
  motorDerecha.stop();                       //detiene los motores
  motorIzquierda.stop();
  debug.println("-> Cambio a estado PARADO");
}

//funcion que calcula el error para rastrear y mover los motores
void calculoError(int inicio, int fin) {
  // cálculo del error
  error_anterior = error;                     // se guarda el error anterior para el control derivativo
  error = 0;                                  // error y número de sensores en negro se inician en 0

  num_negros = 0;
  for (int i = inicio; i < fin; i++) {
    if (bitRead(values, i) == HIGH) {         // si el sensor ha detectado negro,
      error += sensor_error[i];               // se suma el error correspondiente a ese sensor y
      num_negros++;                           // se incrementa el número de sensores que detectan negro
    }
  }

  if (num_negros != 0) {                      // sólo si se ha detectado algún negro,
    error /= num_negros;                      // se aplica la corrección al error para el caso de uno o dos sensores detectando negro
  }

  // aplicación del control PD
  desfase = KP * error + KD * (error - error_anterior);

  velocidad_der = constrain(VELOCIDAD + desfase, -150, 255);
  velocidad_izq = constrain(VELOCIDAD - desfase, -150, 255);


  //actualización de velocidad en motores (comentados para que no se mueva el coche)
  motorDerecha.move(velocidad_der);
  motorIzquierda.move(velocidad_izq);

}
